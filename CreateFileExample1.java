import java.io.File;  
import java.io.IOException;  
import java.io.FileWriter;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class CreateFileExample1   
{  
public static void main(String[] args)   
{
String fileName = new SimpleDateFormat("yyyyMMddHHmm'.txt'").format(new Date());
File file = new File("C:\\Program Files (x86)\\Jenkins\\workspace\\FTC Releases\\jenkins-dev\\copy_file\\music.txt"); //initialize File object and passing path as argument  
boolean result;  
try   
{
if (file.exists())
{
if(file.delete()) 
{ 
System.out.println("File deleted successfully"); 
} 
else
{ 
System.out.println("Failed to delete the file"); 
} 
}
result = file.createNewFile();  //creates a new file  
if(result)      // test if successfully created a new file  
{

        FileWriter fr = null;
        try {
            fr = new FileWriter(file);
            fr.write(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            //close resources
            try {
                fr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


System.out.println("file created "+file.getCanonicalPath()); //returns the path string  
}  
else  
{  
System.out.println("File already exist at location: "+file.getCanonicalPath());  
}  
}   
catch (IOException e)   
{  
e.printStackTrace();    //prints exception if any  
}         
}  
} 